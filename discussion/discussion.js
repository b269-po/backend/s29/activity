// Insert Documents
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: ["CSS", "JavaScript", "Python"],
        department: "HR"
    },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    },
]);

// Insert embedded array
db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

// [SECTION] Comparison Operators
// gt/gte operator
/*
    db.collectionName.find({ field: {$gt: value} });
    db.collectionName.find({ field: {$gte: value} });
*/
db.users.find({ age: {$gt: 65} });

db.users.find({ age: {$gte: 65} });

// lt/lte operator
/*
    db.collectionName.find({ field: {$lt: value} });
    db.collectionName.find({ field: {$lte: value} });
*/

db.users.find({ age: {$lt: 65} }); // Jane
 
db.users.find({ age: {$lte: 65} }); // Bill and Jane

// $ne operator
/*
    db.collectionName.find({ field: {$ne: value} });
*/
db.users.find({ age: {$ne: 82} });

// $in operator
/*
SYNTAX:
    db.collectionName.find({ field: {$in: value} });
*/
db.users.find({ courses: {$in: ["HTML", "React"]} });

// [SECTION] Logical Operators
// $or operator
/*
SYNTAX:
    db.collectionName.find({ $or: [{ fieldA: valueA}, {fieldB: valueB} ] });
*/
db.users.find({ $or: [{ firstName: "Neil"}, {age: 25} ] });

db.users.find({ $or: [{ firstName: "Neil"}, {age: {$gte: 25}} ] });


// $and operator
/*
SYNTAX:
    db.collectionName.find({ $and: [{ fieldA: valueA}, {fieldB: valueB} ] });
*/

db.users.find({ $and: [{ age: {$ne: 82} }, {age: {$ne: 76}} ] });

// department: "HR" and courses: "React"
db.users.find({ $and: [{ department: "HR"}, {courses: {$in: ["React"]} } ] 
});


// [SECTION] Field Projection
// Inclusion
// The value provided is 1 denote that the field is being included
/*
SYNTAX:
    db.collectionName.find({ criteria }, {field: 1});
*/
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);


// Exclusion
// The value provided is 0 denote that the field is being excluded
/*
SYNTAX:
    db.collectionName.find({ criteria }, {field: 0});
*/
db.users.find(
    {firstName: "Jane"},
    {
        contact: 0,
        department: 0
    }
);


// Suppressing the ID field
// Field inclusion and exclusion may not be used at the same time.
// Excluding "_id" is the only exception to this rule
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1,
        lastName: 1,
        contact: 1,
        _id: 0
    }
);


// Returning specific fields in embedded documents
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1,
        lastName: 1,
        "contact.phone" : 1
    }
);


// Project specific array elements in the returned array
db.users.find(
    { "namearr": 
        { 
            namea: "juan"
        }
    },
    { namearr:
        {$slice: 1}
    }
);

// [SECTION] Evaluation Query Operators
// $regex operator
/*
SYNTAX:
    db.collectionName.find({ field: {$regex: 'pattern'});
    db.collectionName.find({ field: {$regex: 'pattern', $options: $optionValue} });
*/

// Case sensitive query
db.users.find({ firstName: {$regex: 'N'} });

// Case insensitive query
db.users.find({ firstName: {$regex: 'N', $options: '$i'} });
